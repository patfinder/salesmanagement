﻿
--part_kind: A, B

-- =====================================================================================================================
-- Data Sanity script
-- =====================================================================================================================

-- ============================================================================
-- Pad sale_no with zero, length = 4

--update customer set sale_no = RIGHT('0000' + ISNULL(sale_no, ''), 4)
--	 where cust_no = 'C00000'

--update tc_detail set sale_no = RIGHT('0000' + ISNULL(sale_no, ''), 4)

-- ============================================================================
-- Delete orphaned customer

--delete from customer where cust_no in (
--	select cust_no
--	from sale_man s 
--		right join customer c on s.sale_no = c.sale_no
--	where s.sale_no is null
--)

-- ============================================================================
-- Delete orphaned tc_detail (no master)

--delete from tc_detail where tc_no in (
--	select d.tc_no
--	from tc_detail d left join tc_master m on d.tc_no = m.tc_no
--	where m.tc_no is null
--)

-- ============================================================================
-- Delete orphaned tc_detail (no product)

--delete from tc_detail where product_no in (
--	select d.product_no
--	from tc_detail d left join product p on d.product_no = p.product_no
--	where p.product_no is null
--)

-- ============================================================================
-- Sanity product table

--select product_no, count(1) from product 
--group by product_no
--having count(1) > 1

-- ============================================================================
-- Sanity details table
--select * from [dbo].[tc_detail]
--where packing_type is null or qty is null

--select * from [dbo].[r_detail]
--where packing_type is null or qty is null

-- =====================================================================================================================
-- Reports
-- =====================================================================================================================

--declare @sale_no nvarchar = '6066'
--declare @cust_no nvarchar = 'C50357'
--declare @tc_date1 datetime = '2010-01-01'
--declare @tc_date2 datetime = '2019-01-01'
--declare @part_kind nvarchar = 'A'


-- ============================================================================
-- Customer Report

/*
SELECT
    m.cust_no,
	SUM(d.packing_type * d.qty) weight,
    SUM(d.qty * d.unit_price) amount,
    p.product_no p_no,
    p.product_vname product_vname,
    d.packing_type,
    p_1,
    p_2
	--p.part_kind
FROM tc_master m
LEFT JOIN tc_detail d
    ON m.tc_no = d.tc_no
LEFT JOIN product p
    ON d.product_no = p.product_no
INNER JOIN customer c
    ON m.cust_no = c.cust_no
WHERE 
m.cust_no = @cust_no
--m.cust_no in ('C50097', 'C50357', 'C50456', 'C50637', 'C50644', 'C50647', 'C50649', 'C50664', 'C50747', 'C50764', 'C50923', 'C50998', 'C51109', 'C51121', 'C51150', 'C51549', 'C51849')
AND tc_date >= @tc_date1
AND tc_date < @tc_date2
--AND (p.part_kind = @part_kind OR @part_kind IS NULL)

GROUP BY 
		 m.cust_no,
		 p_1,
         p_2,
         p.product_no,
         p.product_vname,
         d.packing_type,
		 --p.part_kind
*/



-- ============================================================================
-- Saleman Report


/*
SELECT
    m.sale_no,
	p.product_no p_no,
    packing_type,
    p_1,
    p_2,
    m.cust_no cust_no,
    SUM(packing_type * qty) weight
FROM tc_master m
LEFT JOIN tc_detail d
    ON m.tc_no = d.tc_no
LEFT JOIN product p
    ON d.product_no = p.product_no
INNER JOIN customer c
    ON c.cust_no = m.cust_no
--WHERE c.sale_no = @sale_no
--AND tc_date >= @tc_date1
--AND tc_date < @tc_date2
--AND p.part_kind = @part_kind
GROUP BY m.sale_no,
         p_1,
		 p_2,
         p.product_no,
         packing_type,
         m.cust_no
WHERE product_margin.the_month <> N''
*/



-- ============================================================================
-- Product Margin Report

/*

SELECT        big_table.p_1, big_table.big_name, medium_table.p_2, medium_table.medium_name, product.product_no, product.product_vname, product_margin.packing_type, product_margin.qty, product_margin.unit_margin, 
                         SUM(product_margin.unit_margin * product_margin.qty) AS margin
FROM            product_margin INNER JOIN
                         product ON product_margin.product_no = product.product_no INNER JOIN
                         medium_table ON product.p_2 = medium_table.p_2 INNER JOIN
                         big_table ON medium_table.p_1 = big_table.p_1
GROUP BY big_table.p_1, medium_table.p_2, product.product_no, big_table.big_name, medium_table.medium_name, product.product_vname, product_margin.packing_type, product_margin.qty, product_margin.unit_margin, 
                         product_margin.the_month
HAVING        (product_margin.the_month = N'2018.08')
ORDER BY big_table.p_1, medium_table.p_2, product.product_no, product_margin.packing_type

*/


-- =====================================================================================================================
-- Misc scripts
-- =====================================================================================================================

/*
--select * from [dbo].[sale_man] 
--	where dir_no = '8000' and chief_no is null
	-- '0004', '0016', '0058', '6002', '6073'

--select * from [dbo].[tc_master]

--select * from [dbo].[sale_man] where dir_no is null and chief_no is not null

-- clean data, clear self-ref chief, dir-no
update sale_man set lead_no = null where lead_no = sale_no
update sale_man set chief_no = null where chief_no = sale_no
update sale_man set dir_no = null where dir_no = sale_no

-- List of lead?
select * from sale_man where sale_no in (
	select lead_no from [dbo].[sale_man] where lead_no is not null -- and chief_no is not null
)

-- select sales without dir or chief // No one with sale-data.
select * from [dbo].[sale_man] where chief_no is null and dir_no is null

-- saleman
select * from [dbo].[sale_man] where chief_no is not null and chief_no != sale_no and is_ok = 1

-- sale "004", not "0004" have many sale data.
select * from tc_master where sale_no in 
	(select sale_no from [dbo].[sale_man] where chief_no is null and dir_no is null)
	-- and sale_no = '004'

update tc_master set sale_no = '0004' where sale_no = '004'
	
-- fix saleman's chief-no
--update s set s.chief_no = l.chief_no
--	from [dbo].[sale_man] s inner join [dbo].[sale_man] l on s.lead_no = l.sale_no
--		where s.chief_no is null and s.lead_no is not null

-- chiefs
select * from [dbo].[sale_man] where chief_no is null and dir_no is not null and dir_no != sale_no
	and is_ok = 1

-- dirs
select * from [dbo].[sale_man] where chief_no is null and (dir_no is null or dir_no = sale_no)
	and is_ok = 1 order by sale_no

	
-- dir with team member acount // 0002: 57, 0041: 45
select sc.dir_no, COUNT(distinct sc.sale_no) from tc_master m inner join sale_man sc on m.sale_no = sc.sale_no
	inner join sale_man d on sc.dir_no = d.sale_no
	group by sc.dir_no
	
-- dir point to itself
select * from [dbo].[sale_man] where dir_no = sale_no

--update sale_man set dir_no = (select sale_no from sale_man s2 where sale_man.chief_no = s2.sale_no) where dir_no is null

--select * from [dbo].[sale_man] where sale_no = '0004'

--select * from [dbo].[sale_man] where sale_no in ('0004', '0332') or chief_no = '0004' 

-- dir: 0002, chief: 0004
--select distinct sale_no from sale_man where dir_no = '0002'
-- 0004 0005 0016 0020 0023 0024 0032 0043 0058 0063 0090 0124 0125 0130 0137 0144 0148 0150 0151 0155 0158 0161 0164 0172 0173 0186 0199 0203 0208 0220 0221 0222 0225 0226 0227 0332 6002 6005 6020 6026 6028 6033 6045 6046 6051 6052 6063 6067 6068 6069 6073 6079 6087

--select * from tc_master m inner join sale_man s on m.sale_no = s.sale_no 
--	where s.chief_no in ('0004', '0005', '0016', '0020', '0023', '0024', '0032', '0043', '0058', '0063', '0090', '0124', '0125', '0130', '0137', '0144', '0148', '0150', '0151', '0155', '0158', '0161', '0164', '0172', '0173', '0186', '0199', '0203', '0208', '0220', '0221', '0222', '0225', '0226', '0227', '0332', '6002', '6005', '6020', '6026', '6028', '6033', '6045', '6046', '6051', '6052', '6063', '6067', '6068', '6069', '6073', '6079', '6087')

select 
	f.sale_no f_no, s.sale_no s_no, sum(d.qty * d.packing_type) 
	--count(1)
	FROM tc_master m INNER JOIN tc_detail d ON m.tc_no = d.tc_no 
	INNER JOIN sale_man S ON m.sale_no = s.sale_no 
	INNER JOIN sale_man f ON s.chief_no = f.sale_no 
	WHERE tc_date >= '2015-09-01' AND tc_date < '2015-11-01'
		AND s.dir_no = '0002'
	group by s.sale_no, f.sale_no
	order by f_no, s_no

-- =============================================================
-- Clear chief_no if its equal the sale_no (of that exact saleman)
-- select * from [dbo].[sale_man] where sale_no = chief_no
update [dbo].[sale_man] set chief_no = null where sale_no = chief_no

-- and Director
-- select * from [dbo].[sale_man] where sale_no = dir_no
update [dbo].[sale_man] set dir_no = null where sale_no = dir_no

-* /

-- =============================================================

-- Chief Report

DECLARE @cust_type NVARCHAR(100)
DECLARE @label_flag NVARCHAR(100)
DECLARE @sale_no NVARCHAR(100) = '6036'

-- Test by group by query
--SELECT sale_no, cust_no, DATEPART(MONTH, tc_date) as s_month, COUNT(tc_no) FROM [dbo].[tc_master]
--	WHERE tc_date >= '2016-01-01' and tc_date < '2017-01-01' AND sale_no IS NOT NULL
--	GROUP BY sale_no, cust_no, DATEPART(MONTH, tc_date)
--	ORDER BY sale_no, cust_no

-- Pivot query
SELECT sale_no, sale_ename, cust_no, 
	[1] M1, [2] M2, [3] M3, [4] M4, [5] M5, [6] M6, [7] M7, [8] M8, [9] M9, [10] M10, [11] M11, [12] M12 FROM
	(SELECT m.sale_no, s.sale_ename, c.cust_no, DATEPART(MONTH, tc_date) as s_month, SUM(packing_type * qty) AS TotalWeight
		FROM [dbo].[tc_master] M
			INNER JOIN [dbo].[tc_detail] D ON M.tc_no = D.tc_no
			INNER JOIN [dbo].[customer] C ON M.cust_no = C.cust_no
			INNER JOIN [dbo].[sale_man] S ON S.sale_no = C.sale_no
		WHERE tc_date >= '2016-01-01' and tc_date < '2017-01-01'
		 AND S.chief_no = @sale_no
		--AND s.sale_no IN (6006, 6011, 6012, 6030, 6032, 6035, 6021)
		AND (@cust_type = C.cust_type OR @cust_type IS NULL)
		AND (@label_flag = C.label_flag OR @label_flag IS NULL)
		GROUP BY m.sale_no, S.sale_ename, c.cust_no, DATEPART(MONTH, tc_date)) Q1
	PIVOT (SUM(TotalWeight) FOR s_month in ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12])) AS PVT1
	ORDER BY sale_no, cust_no
*/

-- =============================================================
-- Generates a manual checkpoint // Flush unsaved pages to disk
--CHECKPOINT

-- Clears clean (unmodified) pages from the buffer pool // Clear cache
--DBCC DROPCLEANBUFFERS 

-- clears execution plans for that database
--DBCC FLUSHPROCINDB WITH NO_INFOMSGS
--DBCC FREEPROCCACHE

--/*



-- =============================================================

-- Director report (Bảng xếp hạng Tiếp thị)

-- Time inclusive (<=) on left side and exclusive (<) on right side.

/*
DECLARE @Range1A DATETIME = '2016-05-01'
DECLARE @Range1B DATETIME = '2016-06-01'
DECLARE @Range2A DATETIME = '2016-06-01'
DECLARE @Range2B DATETIME = '2016-07-01'

-- Begin of selected date: Jul 17
DECLARE @theDay DATETIME = '2016-06-17'

DECLARE @dir_no NVARCHAR(50) = '0002'
--DECLARE @sale_no NVARCHAR(50) = '6066'
DECLARE @label_flag NVARCHAR(50)
DECLARE @p_1 NVARCHAR(50)
DECLARE @p_2 NVARCHAR(50)
DECLARE @product_no NVARCHAR(50)
*/

/*

SELECT chief_no, chief_ename, sale_no, sale_ename, mobile,
	SUM(LastPeriod) LastPeriod,
	SUM(ThisDay) ThisDay,
	SUM(ThisPeriod) ThisPeriod,
	SUM(ThisPeriod - LastPeriod) AS Offset,
	SUM(ThisPeriod - LastPeriod) / SUM(LastPeriod) AS OffsetPercent 
FROM
	(SELECT sale_no, sale_ename, mobile,
		chief_no, chief_ename, cust_no, ISNULL([LastPeriod], 0) [LastPeriod],
		ISNULL(ThisDay, 0) ThisDay,
		ISNULL(ThisPeriod, 0) + ISNULL(ThisDay, 0) ThisPeriod 
	FROM
		(SELECT s.sale_no, s.sale_ename, s.mobile,
			s.chief_no, f.sale_ename chief_ename, m.cust_no,
			CASE WHEN tc_date >= @Range1A AND tc_date < @Range1B THEN 'LastPeriod'
				-- Exclude the day
				 WHEN tc_date >= @Range2A AND tc_date < @theDay THEN 'ThisPeriod'
				 WHEN tc_date >= @theday AND tc_date < @Range2B THEN 'ThisDay'
				 ELSE 'Other' END AS RangeType,
			(packing_type * qty) AS TotalWeight
		FROM [dbo].[tc_master] m
			INNER JOIN customer c ON m.cust_no = c.cust_no
			INNER JOIN sale_man s ON c.sale_no = s.sale_no AND s.dir_no = @dir_no
			INNER JOIN sale_man f ON s.chief_no = f.sale_no
			INNER JOIN [dbo].[tc_detail] d ON m.tc_no = d.tc_no
			INNER JOIN product p ON d.product_no = p.product_no
		WHERE tc_date >= @Range1A AND tc_date < @Range2B
			AND (c.label_flag = @label_flag OR @label_flag IS NULL)
			AND (p.p_1 = @p_1 OR @p_1 IS NULL)
			AND (p.p_2 = @p_2 OR @p_2 IS NULL)
			AND (p.product_no = @product_no OR @product_no IS NULL)
		) Q1
	PIVOT (SUM(TotalWeight) FOR RangeType IN ([LastPeriod], [ThisDay], [ThisPeriod])) AS PVT1) PVT 
GROUP BY chief_no, chief_ename, sale_no, sale_ename, mobile 
ORDER BY chief_ename, sale_ename
-- */

/*
SELECT
  p.product_no p_no,
  packing_type packing_type,
  p_1,
  p_2,
  m.cust_no cust_no,
  SUM(qty) qty
FROM tc_master m
LEFT JOIN tc_detail d
  ON m.tc_no = d.tc_no
LEFT JOIN product p
  ON d.product_no = p.product_no
INNER JOIN customer c
  ON c.cust_no = m.cust_no
WHERE c.sale_no = @sale_no
AND tc_date >= @tc_date1
AND tc_date < @tc_date2
GROUP BY p_1,
         p_2,
         p.product_no,
         packing_type,
         m.cust_no
*/

-- =============================================================

/*
-- Test query: Pivot time for a Quarter
SELECT sale_no, [LastPeriod],  [ThisDay], [ThisPeriod],
	[ThisPeriod] - [LastPeriod] AS Offset, ([ThisPeriod] - [LastPeriod]) / [LastPeriod] AS Offset FROM 
	(SELECT s.sale_no,
		CASE WHEN tc_date < @tc_middle THEN 'LastPeriod'
			 WHEN tc_date < @theDay THEN 'ThisDay'
			 ELSE 'ThisPeriod' END AS RangeType,
		(packing_type * qty) AS TotalWeight
	FROM [dbo].[tc_master] m
		INNER JOIN [dbo].[tc_detail] d ON m.tc_no = d.tc_no
		INNER JOIN customer c ON m.cust_no = c.cust_no 
		INNER JOIN sale_man s ON c.sale_no = s.sale_no AND s.dir_no = @dir_no 
	WHERE tc_date >= @tc_date1 AND tc_date < @tc_date2) Q1
	PIVOT (SUM(TotalWeight) FOR RangeType IN ([LastPeriod],  [ThisDay], [ThisPeriod])) AS PVT1
*/

-- =============================================================
-- ------------- Aug 08, 2018

-- =============================================================
-- -------------
-- Product Margin report
/*
 
SELECT big_table.p_1, big_table.big_name, medium_table.p_2, medium_table.medium_name, 
	product.product_no, product.product_vname, product_margin.packing_type, 
	product_margin.qty, product_margin.unit_margin, 
	SUM(product_margin.unit_margin * product_margin.qty) AS margin 
FROM product_margin 
	INNER JOIN product ON product_margin.product_no = product.product_no 
	INNER JOIN medium_table ON product.p_2 = medium_table.p_2 
	INNER JOIN big_table ON medium_table.p_1 = big_table.p_1 
GROUP BY big_table.p_1, medium_table.p_2, product.product_no, big_table.big_name, 
	medium_table.medium_name, product.product_vname, product_margin.packing_type, 
	product_margin.qty, product_margin.unit_margin, product_margin.the_month 
HAVING        (product_margin.the_month = @the_month) 
ORDER BY big_table.p_1, medium_table.p_2, 
	product.product_no, product_margin.packing_type 

*/
-- =============================================================

-- Customer report (Aug 2018)

declare @sale_no nvarchar = '6066'
declare @cust_no nvarchar = 'C50357'
declare @tc_date1 datetime = '2010-01-01'
declare @tc_date2 datetime = '2019-01-01'
declare @part_kind nvarchar = null

SELECT
    SUM(d.packing_type * d.qty) weight,
    SUM(d.qty * d.unit_price - rd.qty * rd.unit_price) amount,
    p.product_no p_no,
    p.product_vname product_vname,
    d.packing_type,
    p.p_1,
    p.p_2
FROM tc_master m
INNER JOIN tc_detail d
    ON m.tc_no = d.tc_no
INNER JOIN product p
    ON d.product_no = p.product_no
FULL JOIN customer c
    ON m.cust_no = c.cust_no
FULL JOIN r_master rm
    ON rm.cust_no = c.cust_no
FULL JOIN r_detail rd
    ON rm.r_no = rd.r_no
    AND rd.product_no = p.product_no
    AND rd.packing_type = d.packing_type
WHERE m.cust_no = @cust_no
AND tc_date >= @tc_date1
AND tc_date < @tc_date2
AND (p.part_kind = @part_kind
OR @part_kind IS NULL)
GROUP BY p.p_1,
         p.p_2,
         p.product_no,
         p.product_vname,
         d.packing_type


-- =============================================================

-- Customer report

DECLARE @cust_no   NVARCHAR(50) = ''
DECLARE @tc_date1  DATETIME = ''
DECLARE @tc_date2  DATETIME = ''
DECLARE @part_kind NVARCHAR(50) = ''

SELECT SUM(weight) weight, SUM(amount) amount,
      p_no, product_vname, packing_type, p_1, p_2
FROM (
    SELECT SUM(- d.packing_type*d.qty) weight, SUM(d.qty * d.unit_price) amount,
      p.product_no p_no, p.product_vname product_vname,
      d.packing_type, p.p_1, p.p_2
    FROM tc_master m
      INNER JOIN tc_detail d ON m.tc_no = d.tc_no
      INNER JOIN product p ON d.product_no = p.product_no
      INNER JOIN customer c ON m.cust_no = c.cust_no
    WHERE m.cust_no = @cust_no
        AND tc_date >= @tc_date1
        AND tc_date < @tc_date2
        AND (p.part_kind = @part_kind OR @part_kind IS NULL)
    GROUP BY p.p_1,
             p.p_2,
             p.product_no,
             p.product_vname,
             d.packing_type

    UNION

    SELECT SUM(- rd.packing_type*rd.qty) weight, 0 amount,
      p.product_no p_no, p.product_vname product_vname,
      rd.packing_type, p.p_1, p.p_2
    FROM r_master rm
      INNER JOIN r_detail rd ON rm.r_no = rd.r_no
      INNER JOIN product p ON rd.product_no = p.product_no
      INNER JOIN customer c ON rm.cust_no = c.cust_no
          AND rd.packing_type = rd.packing_type
    WHERE rm.cust_no = @cust_no
        AND r_date >= @tc_date1
        AND r_date < @tc_date2
        AND (p.part_kind = @part_kind OR @part_kind IS NULL)
    GROUP BY p.p_1,
             p.p_2,
             p.product_no,
             p.product_vname,
             rd.packing_type
) Q1
GROUP BY p_no, product_vname, packing_type, p_1, p_2

