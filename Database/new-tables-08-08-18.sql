USE [Saleman]
GO
/****** Object:  Table [dbo].[product_margin]    Script Date: 8/8/2018 10:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product_margin](
	[the_month] [nvarchar](10) NULL,
	[product_no] [nvarchar](50) NULL,
	[packing_type] [nvarchar](50) NULL,
	[qty] [real] NULL,
	[unit_margin] [real] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[r_detail]    Script Date: 8/8/2018 10:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[r_detail](
	[r_no] [nvarchar](50) NOT NULL,
	[s_no] [nvarchar](50) NULL,
	[product_no] [nvarchar](50) NULL,
	[packing_type] [nvarchar](50) NULL,
	[qty] [real] NULL,
	[unit_price] [real] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[r_master]    Script Date: 8/8/2018 10:48:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[r_master](
	[r_no] [nvarchar](50) NOT NULL,
	[r_date] [datetime] NULL,
	[cust_no] [nvarchar](50) NULL,
	[p_no] [nvarchar](50) NULL,
	[sale_no] [nvarchar](50) NULL,
 CONSTRAINT [PK_r_master] PRIMARY KEY CLUSTERED 
(
	[r_no] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[product_margin] ([the_month], [product_no], [packing_type], [qty], [unit_margin]) VALUES (N'2018.08', N'V801', N'5', 150, 1339)
GO
INSERT [dbo].[product_margin] ([the_month], [product_no], [packing_type], [qty], [unit_margin]) VALUES (N'2018.08', N'V801', N'25', 14675, 2043)
GO
INSERT [dbo].[product_margin] ([the_month], [product_no], [packing_type], [qty], [unit_margin]) VALUES (N'2018.08', N'V801', N'25', 14675, 2043)
GO
INSERT [dbo].[product_margin] ([the_month], [product_no], [packing_type], [qty], [unit_margin]) VALUES (N'2018.08', N'V101', N'25', 11550, 1856)
GO
INSERT [dbo].[product_margin] ([the_month], [product_no], [packing_type], [qty], [unit_margin]) VALUES (N'2018.08', N'V101', N'5', 225, 1235)
GO
INSERT [dbo].[r_detail] ([r_no], [s_no], [product_no], [packing_type], [qty], [unit_price]) VALUES (N'r180802734', N'1', N'H6230', N'25', 61, 258674)
GO
INSERT [dbo].[r_detail] ([r_no], [s_no], [product_no], [packing_type], [qty], [unit_price]) VALUES (N'r180802734', N'2', N'V6320', N'25', 3, 249458)
GO
INSERT [dbo].[r_detail] ([r_no], [s_no], [product_no], [packing_type], [qty], [unit_price]) VALUES (N'r180802734', N'3', N'H6270', N'25', 22, 249215)
GO
INSERT [dbo].[r_master] ([r_no], [r_date], [cust_no], [p_no], [sale_no]) VALUES (N'r180802734', CAST(N'2018-08-07 00:00:00.000' AS DateTime), N'C12206', N'02734', N'0318')
GO
