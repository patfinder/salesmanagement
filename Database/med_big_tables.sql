--USE [Saleman]
GO
/****** Object:  Table [dbo].[big_table]    Script Date: 6/16/2016 9:56:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[big_table](
	[p_1] [nvarchar](50) NOT NULL,
	[big_name] [nvarchar](100) NOT NULL,
 CONSTRAINT [big_table_pk] PRIMARY KEY CLUSTERED 
(
	[p_1] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[medium_table]    Script Date: 6/16/2016 9:56:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[medium_table](
	[p_2] [nvarchar](50) NOT NULL,
	[medium_name] [nvarchar](100) NOT NULL,
	[p_1] [nvarchar](50) NOT NULL,
 CONSTRAINT [medium_table_pk] PRIMARY KEY CLUSTERED 
(
	[p_2] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[big_table] ([p_1], [big_name]) VALUES (N'03', N'Gà đẻ')
INSERT [dbo].[big_table] ([p_1], [big_name]) VALUES (N'04', N'Cút')
INSERT [dbo].[big_table] ([p_1], [big_name]) VALUES (N'05', N'Vịt')
INSERT [dbo].[big_table] ([p_1], [big_name]) VALUES (N'06', N'Gà thịt')
INSERT [dbo].[big_table] ([p_1], [big_name]) VALUES (N'08', N'Heo')
INSERT [dbo].[big_table] ([p_1], [big_name]) VALUES (N'09', N'Bò')
INSERT [dbo].[big_table] ([p_1], [big_name]) VALUES (N'11', N'Dinh dưỡng')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0301', N'Gà đẻ', N'03')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0401', N'Cút', N'04')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0501', N'Vịt thịt', N'05')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0502', N'Vịt đẻ', N'05')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0601', N'Gà công nghiệp', N'06')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0602', N'Gà ta, tàu', N'06')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0801', N'Heo con', N'08')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0802', N'Heo lai', N'08')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0803', N'Heo thịt', N'08')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0804', N'Nái chửa, nái hậu bị', N'08')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0805', N'Nái nuôi con', N'08')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0901', N'Bò sữa', N'09')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0902', N'Bò con', N'09')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'0903', N'Bò thịt', N'09')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1100', N'Dinh dưỡng', N'11')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1101', N'Heo con (DD)', N'11')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1102', N'Heo lai (DD)', N'11')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1103', N'Heo thịt (DD)', N'11')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1104', N'Heo nái (DD)', N'11')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1106', N'Gà đẻ (DD)', N'11')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1107', N'Gà Công nghiệp (DD)', N'11')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1108', N'Gà ta, tàu (DD)', N'11')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1111', N'Vịt thịt (DD)', N'11')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1112', N'Vịt đẻ (DD)', N'11')
INSERT [dbo].[medium_table] ([p_2], [medium_name], [p_1]) VALUES (N'1113', N'Bò Sữa (DD)', N'11')
