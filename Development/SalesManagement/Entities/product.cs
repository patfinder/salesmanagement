﻿namespace SalesManagement.Entities
{
    public class product
    {
        public string p_no { get; set; }

        public string product_vname { get; set; }

        public string part_kind { get; set; }

        public double packing_type { get; set; }

        public string p_1 { get; set; }

        public string p_2 { get; set; }
    }
}