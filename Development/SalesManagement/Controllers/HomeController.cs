﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
//using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Claims;
using System.Web;
//using System.Web.Http;
using System.Web.Mvc;
using Dapper;
using log4net;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json;
//using Oracle.ManagedDataAccess.Client;
using SalesManagement.Entities;
using SalesManagement.Models;
using SalesManagement.Utils;
using VisitMeServer.Admin.Models;
using VisitMeServer.API.Models;
using VisitMeServer.Models;
using WebGrease.Css.Extensions;

namespace SalesManagement.Controllers
{
    [LogActionFilter]
    //[ExceptionLogFilter]
    public class HomeController : Controller // SalesController // Controller
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(HomeController));

        Dictionary<PeriodType, string> Periods = new Dictionary<PeriodType, string>
        {
            {PeriodType.Daily, "month"},
            {PeriodType.Weekly, "week"},
            {PeriodType.Monthly, "month"},
            {PeriodType.Quarterly, "quarter"},
            {PeriodType.Yearly, "year"},
        };

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        public ApplicationSignInManager SignInManager
            => _signInManager ?? (_signInManager = HttpContext.GetOwinContext().Get<ApplicationSignInManager>());

        public ApplicationUserManager UserManager
            => _userManager ?? (_userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>());

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            //base.pro

            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model)
        {
            if (model == null)
                return new HttpStatusCodeResult(HttpStatusCode.Unauthorized);

            using (IDbConnection db =
                new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                //var saleMan =
                //    db.Query<sale_man>(_T("SELECT * FROM sale_man WHERE sale_no = #sale_no and sale_no = #sale_ename"),
                //        new {sale_no = model.Email, sale_ename = model.Password}).FirstOrDefault();

                var saleMan =
                    db.Query<sale_man>(
                        //_T("SELECT * FROM sale_man WHERE is_ok = '1' AND sale_no = #sale_no AND password = #password AND (imei = #imei OR sale_no = imei)"),
                        (AppSettings.IsDebugging && model.Password == AppSettings.DebugPassword ?
                        _T("SELECT * FROM sale_man WHERE sale_no = #sale_no ") :
                        _T( "SELECT * FROM sale_man " + 
                            "WHERE is_ok = '1' AND sale_no = #sale_no COLLATE Latin1_General_CI_AS " + 
                            "    AND password = #password COLLATE Latin1_General_CI_AS " +
                            "    AND imei = #imei COLLATE Latin1_General_CI_AS")),
                        new { sale_no = model.Email, password = model.Password, imei = model.Imei }).FirstOrDefault();

                db.Close();

                if (saleMan == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.NotFound);
                    //return Content(JsonConvert.SerializeObject(ApiResult.NotFound("Sale-man not found")), "application/json");
                }

                if (AppSettings.GeneralDirSaleNo.Contains(saleMan.sale_no))
                {
                    saleMan.role = sale_role.gen;
                }
                // Get role if not general director
                else
                {
                    var roles =
                        db.Query<sale_man>(_T(
                            "SELECT distinct lead_no, chief_no, dir_no FROM sale_man WHERE sale_no != #sale_no " +
                            "   AND (lead_no = #sale_no OR chief_no = #sale_no OR dir_no = #sale_no)"
                        ), new { sale_no = model.Email }).ToList();

                    saleMan.role = sale_role.sale;
                    if (roles.Any(u => u.dir_no == model.Email) || AppSettings.SupportingDirBrands.ContainsKey(saleMan.sale_no))
                    {
                        saleMan.role = sale_role.dir;
                    }
                    else if (roles.Any(u => u.chief_no == model.Email))
                    {
                        saleMan.role = sale_role.chief;
                    }
                    else if (roles.Any(u => u.lead_no == model.Email))
                    {
                        saleMan.role = sale_role.lead;
                    }
                    else
                    {
                        saleMan.role = sale_role.sale;
                    }
                }

                var identity = new ClaimsIdentity(new[]
                {
                    new Claim(ClaimTypes.Name, saleMan.sale_ename),
                    new Claim(ClaimTypes.NameIdentifier, saleMan.sale_no),
                    new Claim(ClaimTypes.Role, saleMan.role.ToString()),
                }, DefaultAuthenticationTypes.ApplicationCookie);

                AuthenticationManager.SignIn(identity);

                return Content(JsonConvert.SerializeObject(saleMan), "application/json");
            }
        }

        [AllowAnonymous]
        public ActionResult Login2(LoginViewModel model)
        {
            var identity = new ClaimsIdentity(new[]
            {
                    new Claim(ClaimTypes.Name, "sale_ename"),
                    new Claim(ClaimTypes.NameIdentifier, "sale_no"),
                    new Claim(ClaimTypes.Role, "role"),
                }, DefaultAuthenticationTypes.ApplicationCookie);

            AuthenticationManager.SignIn(identity);

            return Json(new { UserName = "User1", Role = "Manager" });
        }

        #region ================================== Salemans APIS ==================================

        private string GetCurrentUserSaleNo()
        {
            return User?.Identity?.GetUserId();
        }

        /// <summary>
        /// Get current user role
        /// </summary>
        /// <returns></returns>
        private sale_role GetCurrentUserRole()
        {
            if (User.IsInRole(sale_role.sale.ToString()))
                return sale_role.sale;
            if (User.IsInRole(sale_role.lead.ToString()))
                return sale_role.lead;
            if (User.IsInRole(sale_role.chief.ToString()))
                return sale_role.chief;
            if (User.IsInRole(sale_role.dir.ToString()))
                return sale_role.dir;
            if (User.IsInRole(sale_role.gen.ToString()))
                return sale_role.gen;

            return sale_role.sale;
        }

        private List<sale_man> GetBelongingDirs()
        {
            var saleNo = GetCurrentUserSaleNo();
            var role = GetCurrentUserRole();

            // sale can't get chiefs, eg.
            if (role >= sale_role.dir)
            {
                //    throw new Exception("Unauthorized action.");
                return new List<sale_man>();
            }

            List<sale_man> dirs;

            using (
                IDbConnection db =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                var sql = "SELECT * FROM sale_man S1 WHERE is_ok = 1 AND EXISTS (SELECT 1 FROM sale_man S2 WHERE s2.dir_no = s1.sale_no) ORDER BY sale_ename ";
                dirs = db.Query<sale_man>(_T(sql)).ToList();
                Log.Debug($"[{GetCurrentUserSaleNo()}] {nameof(GetBelongingDirs)} - sql: {sql}");
            }

            return dirs.Where(s => s.sale_no != saleNo).ToList();
        }

        /// <summary>
        /// Get chiefs belonging to specified supervisor (or current user)
        /// </summary>
        /// <param name="supervisorId">Chiefs' supervisor</param>
        /// <returns></returns>
        private List<sale_man> GetBelongingChiefs(string supervisorId = null)
        {
            // If supervisor ID provided, currrent user is supposed to be Supporting Dir
            var role = sale_role.dir;

            if (string.IsNullOrEmpty(supervisorId))
            {
                supervisorId = GetCurrentUserSaleNo();
                role = GetCurrentUserRole();
            }

            // sale can't get chiefs, eg.
            if (role >= sale_role.sale)
            {
                //    throw new Exception("Unauthorized action.");
                return new List<sale_man>();
            }

            List<sale_man> chiefs;

            using (
                IDbConnection db =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                var sql = "";
                // Find any chief
                if (role == sale_role.gen)
                {
                    sql = $"SELECT sale_no, sale_ename FROM sale_man s1 WHERE is_ok = '1' AND " +
                        // Chief of some others
                        " EXISTS (SELECT 1 FROM sale_man s2 WHERE s1.sale_no = s2.chief_no AND s2.sale_no != s1.sale_no) " +
                        // Exclude dirs
                        " AND NOT EXISTS (SELECT 1 FROM sale_man S3 WHERE s3.dir_no = s1.sale_no AND S3.sale_no != s1.sale_no) " +
                        " ORDER BY sale_ename ";
                    chiefs = db.Query<sale_man>(_T(sql)).ToList();
                }
                else
                {
                    sql = $"SELECT sale_no, sale_ename FROM sale_man s1 WHERE is_ok = '1' AND dir_no = #supervisorId AND " +
                        // Chief of some others
                        " EXISTS (SELECT 1 FROM sale_man S3 WHERE s3.chief_no = s1.sale_no AND S3.sale_no != s1.sale_no) " +
                        " ORDER BY sale_ename ";
                    chiefs = db.Query<sale_man>(_T(sql), new { supervisorId }).ToList();
                }

                Log.Debug($"[{GetCurrentUserSaleNo()}] {nameof(GetBelongingChiefs)} - sql: {sql}");
            }

            return chiefs.Where(s => s.sale_no != supervisorId).ToList();
        }

        /// <summary>
        /// Get sales / chiefs / dirs belonging to current user.
        /// Used for getting sale-man list on UI.
        /// </summary>
        /// <returns></returns>
        private List<sale_man> GetBelongingSalemans(string supervisorId = null)
        {
            // If supervisor ID provided, currrent user is supposed to be Supporting Dir
            var role = sale_role.dir;

            if (string.IsNullOrEmpty(supervisorId))
            {
                supervisorId = GetCurrentUserSaleNo();
                role = GetCurrentUserRole();
            }

            // sale can't get other sales, eg.
            if (role >= sale_role.sale)
            {
                //    throw new Exception("Unauthorized action.");
                return new List<sale_man>();
            }

            using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // Add dir/chief/lead role check if need.
                var roleCondStr = role != sale_role.gen ? $"AND {role}_no = '{supervisorId}'" : null;

                var sql = $"SELECT sale_no, sale_ename FROM sale_man S1 WHERE is_ok = '1' {roleCondStr}" +
                    // Exclude chiefs
                    //" AND NOT EXISTS (SELECT 1 FROM sale_man S2 WHERE s2.chief_no = S1.sale_no AND S2.sale_no != S1.sale_no) " +
                    " ORDER BY sale_ename";
                List<sale_man> sales = db.Query<sale_man>(_T(sql)).ToList();
                Log.Debug($"[{GetCurrentUserSaleNo()}] {MethodBase.GetCurrentMethod().Name} - sql: {sql}");

                return sales.Where(s => s.sale_no != supervisorId).ToList();
            }
        }

        /// <summary>
        /// Get sale-man/chief/dir under specified user
        /// </summary>
        /// <returns></returns>
        private List<string> GetBelongingSalemanIds(string supervisorId = null)
        {
            var dirIds = new List<string>();
            var chiefIds = new List<string>();
            var leadIds = new List<string>();
            var saleIds = new List<string>();

            // If supervisor ID provided, currrent user is supposed to be Supporting Dir
            var role = sale_role.dir;

            if (string.IsNullOrEmpty(supervisorId))
            {
                supervisorId = GetCurrentUserSaleNo();
                role = GetCurrentUserRole();
            }

            using (
                IDbConnection db =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                var sql = "";

                // Gen
                if (role == sale_role.gen)
                {
                    sql = "SELECT sale_no FROM sale_man WHERE is_ok = '1'";
                    saleIds = db.Query<string>(sql).ToList();
                }
                else
                {
                    // Dir - get current user's subordinate (thuộc cấp)
                    if (role == sale_role.dir)
                    {
                        dirIds.Add(supervisorId);
                        sql = "SELECT sale_no FROM sale_man WHERE is_ok = '1' AND dir_no = #dir_no";
                        chiefIds = db.Query<string>(_T(sql),
                            new { dir_no = dirIds.First() }).ToList();
                    }

                    // Chiefs - get current user's subordinate (thuộc cấp)
                    if (role == sale_role.chief)
                        chiefIds.Add(supervisorId);

                    if (chiefIds.Any())
                    {
                        var saleList = string.Join(",", chiefIds.Select(id => $"'{id}'"));
                        sql = $"SELECT sale_no FROM sale_man WHERE is_ok = '1' AND chief_no IN ({saleList})";
                        leadIds = db.Query<string>(sql).ToList();
                    }

                    // Leads - get current user's subordinate (thuộc cấp)
                    if (role == sale_role.lead)
                        leadIds.Add(supervisorId);

                    if (leadIds.Any())
                    {
                        var saleList = string.Join(",", leadIds.Select(id => $"'{id}'"));
                        sql = $"SELECT sale_no FROM sale_man WHERE is_ok = '1' AND lead_no IN ({saleList})";
                        saleIds = db.Query<string>(sql).ToList();
                    }
                }

                Log.Debug($"[{GetCurrentUserSaleNo()}] {MethodBase.GetCurrentMethod().Name} - sql: {sql}");

                db.Close();
            }

            // Combine chiefs, lead, sales
            return chiefIds.Union(leadIds).Union(saleIds).ToList();
        }

        /// <summary>
        /// Get customers belong to one the specified saleman
        /// </summary>
        /// <param name="saleIds"></param>
        /// <returns></returns>
        private List<customer> GetSalesCustomers(string[] saleIds)
        {
            List<customer> customers;

            using (
                IDbConnection db =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                var saleList = string.Join(",", saleIds.Select(id => $"'{id}'"));

                var sql = $"SELECT * FROM customer WHERE is_ok = '1' AND sale_no IN ({string.Join(",", saleList)}) ORDER BY cust_no";
                customers = db.Query<customer>(sql).ToList();
                Log.Debug($"[{GetCurrentUserSaleNo()}] {MethodBase.GetCurrentMethod().Name} - sql: {sql}");

                db.Close();
            }

            return customers;
        }

        /// <summary>
        /// Get customer list for current user.
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult GetCustomerList()
        {
            var supervisorId = AppSettings.SupportingDirs.ContainsKey(GetCurrentUserSaleNo())
                ? AppSettings.SupportingDirs[GetCurrentUserSaleNo()]
                : null;

            var saleIds = GetBelongingSalemanIds(supervisorId);
            saleIds.Add(GetCurrentUserSaleNo());

            var customers = GetSalesCustomers(saleIds.ToArray());

            return Content(JsonConvert.SerializeObject(new ApiResult<object>
            {
                Result = customers,
                ResultCode = ApiResultCode.Success,
            }), "application/json");
        }

        /// <summary>
        /// API for gettings belonging sale-mans
        /// </summary>
        /// <returns></returns>
        public ActionResult GetSalemanList()
        {
            var supervisorId = AppSettings.SupportingDirs.ContainsKey(GetCurrentUserSaleNo())
                ? AppSettings.SupportingDirs[GetCurrentUserSaleNo()]
                : null;

            var saleMans = GetBelongingSalemans(supervisorId);

            return Content(JsonConvert.SerializeObject(new ApiResult<object>
            {
                Result = saleMans.Select(s => new { s.sale_no, s.sale_ename }),
                ResultCode = ApiResultCode.Success,
            }), "application/json");
        }

        public ActionResult GetChiefList()
        {
            var supervisorId = AppSettings.SupportingDirs.ContainsKey(GetCurrentUserSaleNo())
                ? AppSettings.SupportingDirs[GetCurrentUserSaleNo()]
                : null;

            var saleMans = GetBelongingChiefs(supervisorId);

            return Content(JsonConvert.SerializeObject(new ApiResult<object>
            {
                Result = saleMans.Select(s => new { s.sale_no, s.sale_ename }),
                ResultCode = ApiResultCode.Success,
            }), "application/json");
        }

        public ActionResult GetDirList()
        {
            var saleMans = GetBelongingDirs();

            return Content(JsonConvert.SerializeObject(new ApiResult<object>
            {
                Result = saleMans.Select(s => new { s.sale_no, s.sale_ename }),
                ResultCode = ApiResultCode.Success,
            }), "application/json");
        }

        #endregion

        #region ================================== Products Catalogs ==================================

        public ActionResult ChangePassword(ChangePasswordModel model)
        {
            var saleNo = GetCurrentUserSaleNo();

            using (IDbConnection db =
                new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                var saleMan =
                    db.Query<sale_man>(
                        _T("SELECT * FROM sale_man WHERE sale_no = #sale_no and password = #password"),
                        new { sale_no = saleNo, password = model.OldPassword }).FirstOrDefault();

                if (saleMan == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

                db.Execute(_T("UPDATE sale_man SET Password = #password WHERE sale_no = #sale_no"),
                    new { sale_no = saleNo, password = model.NewPassword });
            }

            return
                Content(
                    JsonConvert.SerializeObject(new ApiResult<object>
                    {
                        ResultCode = ApiResultCode.Success,
                        ResultMessages = new List<string> { "Update password successfully" }
                    }), "application/json");
        }

        public ActionResult GetLabelFlags()
        {
            var labelFlags = new Dictionary<int, string>
            {
                //{0, "Type 0"},
                {1, "Nupak"},
                {2, "Nutrition"},
                {3, "Dachan"},
                //{4, "Type 4"},
                {5, "Redstar"},
                //{6, "Type 6"},
                //{8, "Type 8"},
                //{9, "Type 9"},
            };

            return Content(JsonConvert.SerializeObject(new ApiResult<object>
            {
                Result = labelFlags,
                ResultCode = ApiResultCode.Success,
            }), "application/json");
        }

        public ActionResult GetP1List()
        {
            try
            {
                using (IDbConnection db =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    var p1List = db.Query<big_table>(_T("SELECT * FROM big_table"),
                        new { }).ToList();

                    db.Close();

                    //p1List = null;
                    return Content(JsonConvert.SerializeObject(new ApiResult<object>
                    {
                        Result = p1List.ToDictionary(p1 => p1.p_1, p1 => p1.big_name),
                        ResultCode = ApiResultCode.Success,
                    }), "application/json");
                }
            }
            catch (Exception ex)
            {
                Log.Error($"[{GetCurrentUserSaleNo()}] {nameof(GetP1List)} exception: {ex}");

                throw;
            }
        }

        public ActionResult GetP2List(string p1)
        {
            using (IDbConnection db =
                new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                var p2List = db.Query<medium_table>(_T("SELECT * FROM medium_table WHERE p_1 = #p_1"), new { p_1 = p1 }).ToList();

                db.Close();

                return Content(JsonConvert.SerializeObject(new ApiResult<object>
                {
                    Result = p2List.Select(p => new
                    {
                        p2 = p.p_2,
                        p2_name = p.medium_name,
                    }),
                    ResultCode = ApiResultCode.Success,
                }), "application/json");
            }
        }

        public ActionResult GetProductList(string p2)
        {
            using (IDbConnection db =
                new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                var produtcs = db.Query<product>(_T("SELECT product_no p_no, product_vname, p_1, p_2, part_kind FROM product WHERE p_2 = #p_2"),
                    new { p_2 = p2 }).ToList();

                db.Close();

                return Content(JsonConvert.SerializeObject(new ApiResult<object>
                {
                    Result = produtcs.Select(p => new
                    {
                        product_no = p.p_no,
                        p.product_vname,
                        p.p_1,
                        p.p_2,
                        p.part_kind,
                    }),
                    ResultCode = ApiResultCode.Success,
                }), "application/json");
            }
        }
        #endregion

        #region ================================== Reports ==================================

        [Authorize]
        public ActionResult GetCustomerReport(CustomerReportModel model)
        {
            try
            {
                List<CommonQueryResult> queryResults;
                customer customer1;

                var sw = new Stopwatch();
                sw.Start();

                using (
                    IDbConnection db =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    // ==================================
                    // Read customer
                    customer1 = db.Query<customer>(_T("SELECT * FROM customer WHERE cust_no = #cust_no"),
                        new
                        {
                            model.cust_no,
                        }).FirstOrDefault();

                    if (customer1 == null)
                    {
                        return Content(JsonConvert.SerializeObject(new ApiResult()
                        {
                            ResultCode = ApiResultCode.ParametersError,
                            ResultMessages = new List<string> { "Customer not found." }
                        }), "application/json");
                    }

                    // ==================================
                    // Read sale data

                    var sql = _T(
                        "SELECT SUM(weight) weight, SUM(amount) amount, " +
                        "      p_no, product_vname, packing_type, p_1, p_2 " +
                        "FROM ( " +
                        "    SELECT SUM(d.packing_type*d.qty) weight, SUM(d.qty * d.unit_price) amount, " +
                        "      p.product_no p_no, p.product_vname product_vname, " +
                        "      d.packing_type, p.p_1, p.p_2 " +
                        "    FROM tc_master m " +
                        "      INNER JOIN tc_detail d ON m.tc_no = d.tc_no " +
                        "      INNER JOIN product p ON d.product_no = p.product_no " +
                        "      INNER JOIN customer c ON m.cust_no = c.cust_no " +
                        "    WHERE m.cust_no = @cust_no " +
                        "        AND tc_date >= @tc_date1 " +
                        "        AND tc_date < @tc_date2 " +
                        "        AND (p.part_kind = @part_kind OR @part_kind IS NULL) " +
                        "    GROUP BY p.p_1, " +
                        "             p.p_2, " +
                        "             p.product_no, " +
                        "             p.product_vname, " +
                        "             d.packing_type " +

                        "    UNION ALL " +

                        "    SELECT SUM(- rd.packing_type*rd.qty) weight, 0 amount, " +
                        "      p.product_no p_no, p.product_vname product_vname, " +
                        "      rd.packing_type, p.p_1, p.p_2 " +
                        "    FROM r_master rm " +
                        "      INNER JOIN r_detail rd ON rm.r_no = rd.r_no " +
                        "      INNER JOIN product p ON rd.product_no = p.product_no " +
                        "      INNER JOIN customer c ON rm.cust_no = c.cust_no " +
                        "          AND rd.packing_type = rd.packing_type " +
                        "    WHERE rm.cust_no = @cust_no " +
                        "        AND r_date >= @tc_date1 " +
                        "        AND r_date < @tc_date2 " +
                        "        AND (p.part_kind = @part_kind OR @part_kind IS NULL) " +
                        "    GROUP BY p.p_1, " +
                        "             p.p_2, " +
                        "             p.product_no, " +
                        "             p.product_vname, " +
                        "             rd.packing_type " +
                        ") Q1 " +
                        "GROUP BY p_no, product_vname, packing_type, p_1, p_2 "
                    );

                    Log.Debug($"[{GetCurrentUserSaleNo()}] {MethodBase.GetCurrentMethod().Name} - sql: {sql}");

                    queryResults = db.Query<CommonQueryResult>(sql,
                        new
                        {
                            model.cust_no,
                            model.part_kind,
                            tc_date1 = model.tc_date1.Date,
                            tc_date2 = model.tc_date2.Date.AddDays(1)
                        }).ToList();

                    db.Close();
                }

                List<ReportP1> p1List = queryResults.GroupBy(r => r.p_1).ToList().Select(g1 => new ReportP1
                {
                    P1 = g1.Key,
                    P2List = g1.GroupBy(p => p.p_2).Select(g2 => new ReportP2
                    {
                        P2 = g2.Key,
                        Products = g2.Select(pi => new ReportProduct
                        {
                            p_no = pi.p_no,
                            product_vname = pi.product_vname,
                            packing_type = pi.packing_type,
                            weight = pi.weight,
                            amount = pi.amount
                        }).ToList()
                    }).ToList()
                }).ToList();

                Debug.WriteLine($"{nameof(GetCustomerReport)} - count: {p1List.Count}");

                // ==================================
                // Calculate Total

                var list = new Dictionary<int, object[]>();
                var row = 1;
                var dataRow = 1;

                foreach (var p1 in p1List)
                {
                    var p1HeaderAdded = false;

                    // P1 row
                    //list.Add(row++, new object[]
                    //{
                    //    p1.P1Name, "", "", "" // , "",
                    //});

                    foreach (var p2 in p1.P2List)
                    {
                        var p2HeaderAdded = false;

                        // P2 row
                        //list.Add(row++, new object[]
                        //{
                        //    p2.P2Name, "", "", "" // , "",
                        //});

                        foreach (var prod in p2.Products)
                        {
                            // Product row
                            list.Add(row++, new object[]
                            {
                                p1HeaderAdded ? "" : p1.P1Name,
                                p2HeaderAdded ? "" : p2.P2Name,
                                dataRow++,
                                //prod.p_no + "\t" + prod.p_vname,
                                prod.p_no,
                                prod.packing_type.ToString("##") + " Kg",
                                prod.weight, // prod.amount,
                            });

                            p1HeaderAdded = true;
                            p2HeaderAdded = true;
                        }

                        // P2 total
                        list.Add(row++, new object[]
                        {
                            //"Total of P2", "", "", p2.TotalWeight // , p2.TotalAmount,
                            "", "", "Total of P2", "", "", p2.TotalWeight // , p2.TotalAmount,
                        });
                    }

                    // P1 total
                    list.Add(row++, new object[]
                    {
                        //"Total of P1", "", "", p1.TotalWeight // , p1.TotalAmount,
                        "", "", "Total of P1", "", "", p1.TotalWeight // , p1.TotalAmount,
                    });
                }

                // Grand Total line
                list.Add(row++, new object[]
                {
                    //"Grand Total", "", "", AgentTotalWeight // , AgentTotalAmount
                    "", "", "Grand Total", "", "", p1List.Sum(p => p.TotalWeight) // , AgentTotalAmount
                });

                // Apply format
                foreach (var item in list.Where(item => item.Value[5] is double))
                {
                    item.Value[5] = ((double)item.Value[5]).ToString("##,###");
                }

                var resultModel2 = new
                {
                    customer1.cust_vname,
                    index = new[]
                    {
                        //"Seq", "Product Name", "Packing type", "Weight (Kgs)" // , "Amount (VND)"
                        "P1", "P2", "Seq", "Product Name", "Packing type", "Weight (Kgs)" // , "Amount (VND)"
                    },
                    data = list
                };

                sw.Stop();
                Log.Info($"[{GetCurrentUserSaleNo()}] {nameof(GetCustomerReport)} time: {sw.ElapsedMilliseconds}ms");

                return
                    Content(
                        JsonConvert.SerializeObject(new ApiResult<object>
                        {
                            Result = resultModel2,
                            ResultCode = ApiResultCode.Success
                        }), "application/json");
            }
            catch (Exception ex)
            {
                return Content("Error: " + ex.Message);
            }
        }

        [Authorize]
        public ActionResult GetSaleManReport(SalemanReportModel model)
        {
            var sw = new Stopwatch();
            sw.Start();

            List<CommonQueryResult> queryResults;

            if (GetCurrentUserRole() >= sale_role.sale || string.IsNullOrEmpty(model.sale_no))
                model.sale_no = GetCurrentUserSaleNo();

            var supervisorId = AppSettings.SupportingDirs.ContainsKey(GetCurrentUserSaleNo())
                ? AppSettings.SupportingDirs[GetCurrentUserSaleNo()]
                : null;

            // Check permission
            if (GetBelongingSalemanIds(supervisorId).All(s => s != model.sale_no) && GetCurrentUserSaleNo() != model.sale_no)
            {
                return Content(JsonConvert.SerializeObject(new ApiResult
                {
                    ResultCode = ApiResultCode.GenericError,
                    ResultMessages = new List<string> { "Unauthorized Access" },
                }), "application/json");
            }

            using (
                IDbConnection db =
                    new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                // ==================================
                // Read customer
                var saleMan = db.Query<sale_man>(_T("SELECT * FROM sale_man WHERE is_ok = '1' AND sale_no = #sale_no"),
                    new
                    {
                        sale_no = model.sale_no ?? GetCurrentUserSaleNo(),
                    }).FirstOrDefault();

                if (saleMan == null)
                    return
                        Content(
                            JsonConvert.SerializeObject(new ApiResult()
                            {
                                ResultCode = ApiResultCode.ParametersError,
                                ResultMessages = new List<string> { "Sale man not found." }
                            }), "application/json");

                // ==================================
                // Read sale data

                // weight is actually count of package
                var sql = _T(
                    "SELECT p_1, p_2, p_no, packing_type, cust_no, SUM(weight) weight " +
                    "FROM ( " +
                    "    SELECT p.product_no p_no, d.packing_type, " +
                    "        p_1, p_2, m.cust_no cust_no, SUM(d.packing_type*d.qty) weight " +
                    "    FROM tc_master m " +
                    "        INNER JOIN tc_detail d ON m.tc_no = d.tc_no " +
                    "        INNER JOIN product p ON d.product_no = p.product_no " +
                    "        INNER JOIN customer c ON c.cust_no = m.cust_no " +
                    "    WHERE c.sale_no = #sale_no AND tc_date >= #tc_date1 AND tc_date < #tc_date2 " +
                    "        AND (p.part_kind = #part_kind OR #part_kind IS NULL) " +
                    "    GROUP BY p_1, p_2, p.product_no, d.packing_type, m.cust_no " +

                    "    UNION ALL " +

                    "    SELECT p.product_no p_no, d.packing_type, " +
                    "        p_1, p_2, m.cust_no cust_no, SUM(- d.packing_type*d.qty) weight " +
                    "    FROM r_master m " +
                    "        INNER JOIN r_detail d ON m.r_no = d.r_no " +
                    "        INNER JOIN product p ON d.product_no = p.product_no " +
                    "        INNER JOIN customer c ON c.cust_no = m.cust_no " +
                    "    WHERE c.sale_no = #sale_no AND r_date >= #tc_date1 AND r_date < #tc_date2 " +
                    "        AND (p.part_kind = #part_kind OR #part_kind IS NULL) " +
                    "    GROUP BY p_1, p_2, p.product_no, d.packing_type, m.cust_no " +
                    ") Q " +
                    "GROUP BY p_1, p_2, p_no, packing_type, cust_no "
                    );

                Log.Debug($"[{GetCurrentUserSaleNo()}] {nameof(GetSaleManReport)} - sql: {sql}");

                queryResults = db.Query<CommonQueryResult>(sql,
                    new
                    {
                        model.sale_no,
                        tc_date1 = model.tc_date1.Date,
                        tc_date2 = model.tc_date2.Date.AddDays(1),
                        model.part_kind,
                    }).ToList();

                db.Close();
            }

            List<SalemanReportP1> p1List = queryResults.GroupBy(g1 => g1.p_1).ToList().Select(g1 => new SalemanReportP1
            {
                P1 = g1.Key,
                P2List = g1.GroupBy(g2 => g2.p_2).Select(g2 => new SalemanReportP2
                {
                    P2 = g2.Key,
                    Products = g2.GroupBy(pi => new { pi.p_no, pi.product_vname, pi.packing_type }).
                        Select(pi => new SalemanReportProduct
                        {
                            p_no = pi.Key.p_no,
                            product_vname = pi.Key.product_vname,
                            packing_type = pi.Key.packing_type,
                            Customers = pi.OrderBy(c => c.cust_no).
                                Select(c => new Tuple2<string, double>
                                {
                                    Item1 = c.cust_no,
                                    Item2 = c.weight
                                }).ToList()
                        }).ToList()
                }).ToList()
            }).ToList();

            Debug.WriteLine($"{nameof(GetSaleManReport)} - count: {queryResults.Count}");

            // ==================================
            // Convert to new format

            var row = 1;
            var list = new Dictionary<int, object[]>();

            var customerList =
                p1List.SelectMany(
                    p1 => p1.P2List.SelectMany(p2 => p2.Products).SelectMany(pi => pi.Customers.Select(c => c.Item1)))
                    .Distinct().OrderBy(c => c);

            var grandSum = customerList.Select(c => .0).ToList();
            // Sum of Grands row
            grandSum.Add(.0);

            foreach (var p1 in p1List)
            {
                // Per customer
                var p1TotalLine = customerList.Select(c => .0).ToList();

                // Total
                p1TotalLine.Add(0);

                for (var j = 0; j < p1.P2List.Count; j++)
                {
                    var p2 = p1.P2List[j];

                    for (var k = 0; k < p2.Products.Count; k++)
                    {
                        var product = p2.Products[k];
                        var columns = new List<object>
                        {
                            j == 0 && k == 0 ? p1.P1Name : "",
                            k == 0 ? p2.P2Name : "",
                            // Product, Packing
                            product.p_no,
                            product.packing_type + " Kg",
                        };

                        // Add customers to columns
                        customerList.ForEach(c =>
                        {
                            var pc = product.Customers.FirstOrDefault(x => x.Item1 == c);
                            // Weight
                            if (pc != null)
                            {
                                columns.Add(pc.Item2);
                            }
                            else
                            {
                                columns.Add("");
                            }
                        });

                        // Line total
                        columns.Add(product.Customers.Sum(c => c.Item2));

                        list.Add(row++, columns.ToArray());
                    }

                    // Sum by customer in P2
                    var custSums = customerList.Select(
                        k => p2.Products.Sum(p => p.Customers.Where(c => c.Item1 == k).Sum(c => c.Item2))).ToList();

                    // Line total column
                    custSums.Add(custSums.Sum(s => s));

                    // P2 total line
                    list.Add(row++, new object[]
                    {
                        "", p2.P2 + " Total", "", ""
                    }
                        // List of sum by customer
                        .Concat(custSums.Select(s => (object)s)).ToArray());

                    // Add to P1 total line
                    for (var k = 0; k < custSums.Count; k++)
                    {
                        p1TotalLine[k] += custSums[k];
                    }
                }

                // P1 Total line
                list.Add(row++, new object[]
                {
                    p1.P1 + " Total", "", "", ""
                }
                    // List of sum by customer
                    .Concat(p1TotalLine.Select(s => (object)s)).ToArray());

                // Add to Grand sum
                for (var p = 0; p < p1TotalLine.Count; p++)
                {
                    grandSum[p] += p1TotalLine[p];
                }
            }

            // Grand Total line
            var grandLine = new object[]
            {
                "Grand Total", "", "", ""
            }
                .Concat(grandSum.ConvertAll(s => (object)s)).ToArray();

            list.Add(row++, grandLine);

            // Apply format
            foreach (var item in list)
            {
                for (var i = 4; i < item.Value.Length; i++)
                {
                    if (item.Value[i] is double)
                        item.Value[i] = ((double)item.Value[i]).ToString("##,###");
                }
            }

            var resultModel2 = new
            {
                index =
                    new[] { "P1", "P2", "Product No", "Packing" }.Concat(customerList).Concat(new[] { "Grand Total" }),
                data = list,
            };

            sw.Stop();
            Log.Info($"[{GetCurrentUserSaleNo()}] {nameof(GetSaleManReport)} time: {sw.ElapsedMilliseconds}ms");

            return
                Content(
                    JsonConvert.SerializeObject(new ApiResult<object>
                    {
                        Result = resultModel2,
                        ResultCode = ApiResultCode.Success
                    }), "application/json");
        }

        [Authorize]
        public ActionResult GetChiefReport(ChiefReportModel model)
        {
            try
            {
                var sw = new Stopwatch();
                sw.Start();

                List<dynamic> detailRows;
                sale_man chief;

                var userRole = GetCurrentUserRole();

                // If this is "self", chiefNo is self's sale_no - otherwise, get requested chief_no
                var isUserLeadOrChief = new[] { sale_role.chief, sale_role.lead }.Contains(userRole);
                var requestForSelf = isUserLeadOrChief || string.IsNullOrEmpty(model.chief_no);
                var chiefNo = requestForSelf ? GetCurrentUserSaleNo() : model.chief_no;

                // Check permission
                if (!string.IsNullOrEmpty(model.label_flag) &&
                    !AppSettings.SupportingDirBrands[GetCurrentUserSaleNo()].Contains(model.label_flag))
                {
                    return Content(JsonConvert.SerializeObject(new ApiResult<object>
                    {
                        Result = null,
                        ResultCode = ApiResultCode.GenericError,
                        ResultMessages = new List<string> { "Unauthorized" }
                    }), "application/json");
                }

                string supervisorId = null;
                string label_flags = null;

                // For supporting dir
                if (AppSettings.SupportingDirs.ContainsKey(GetCurrentUserSaleNo()))
                {
                    supervisorId = AppSettings.SupportingDirs[GetCurrentUserSaleNo()];
                    label_flags = string.IsNullOrEmpty(model.label_flag) ?
                        model.label_flag :
                        string.Join(", ", AppSettings.SupportingDirBrands[GetCurrentUserSaleNo()].Select(s => $"'{s}'"));
                }

                // Ignore Area for Chief/Lead user.
                if (isUserLeadOrChief)
                    model.cust_type = null;

                var year = model.tc_date.Year;
                var monthCount = model.tc_date.Year == DateTime.Now.Year ? DateTime.Now.Month : 12;

                // Unauthorized access
                if (userRole > sale_role.lead ||
                    userRole <= sale_role.lead && GetCurrentUserSaleNo() != chiefNo && GetBelongingChiefs(supervisorId).All(s => s.sale_no != chiefNo))
                {
                    return Content(JsonConvert.SerializeObject(new ApiResult
                    {
                        ResultCode = ApiResultCode.GenericError,
                        ResultMessages = new List<string> { "Unauthorized Access" },
                    }), "application/json");
                }

                using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    // ==================================
                    // Read customer
                    chief = db.Query<sale_man>(_T("SELECT * FROM sale_man WHERE is_ok = '1' AND sale_no = #sale_no"), new
                    {
                        sale_no = chiefNo,
                    }).FirstOrDefault();

                    if (chief == null)
                    {
                        return Content(JsonConvert.SerializeObject(new ApiResult()
                        {
                            ResultCode = ApiResultCode.ParametersError,
                            ResultMessages = new List<string> { "Sale man not found." }
                        }), "application/json");
                    }

                    // ==================================
                    // Get Sales list

                    var saleNos = GetBelongingSalemanIds(supervisorId);
                    saleNos.Insert(0, GetCurrentUserSaleNo());
                    //var saleNosStr = string.Join(",", saleNos);

                    var yearStart = new DateTime(model.tc_date.Year, 1, 1);
                    var yearEnd = yearStart.AddYears(1);

                    var label_flagsCond = label_flags == null ? "" : $" AND C.label_flag IN ({label_flags}) ";

                    var sql = _T(
                        "SELECT sale_no, sale_ename, cust_no, " +
                        "	[1] M1, [2] M2, [3] M3, [4] M4, [5] M5, [6] M6, " +
                        "    [7] M7, [8] M8, [9] M9, [10] M10, [11] M11, [12] M12 " +
                        "FROM ( " +
                        "    SELECT sale_no, sale_ename, cust_no, s_month, SUM(TotalWeight) TotalWeight " +
                        "    FROM ( " +
                        "        SELECT C.sale_no, S.sale_ename, C.cust_no, DATEPART(MONTH, tc_date) AS s_month, SUM(D.packing_type * D.qty) AS TotalWeight " +
                        "        FROM tc_master M " +
                        "            INNER JOIN tc_detail D ON M.tc_no = D.tc_no " +
                        "            INNER JOIN customer C ON M.cust_no = C.cust_no " +
                        "            INNER JOIN sale_man S ON S.sale_no = C.sale_no " +
                        "        WHERE tc_date >= #tc_date1 and tc_date < #tc_date2 " +
                        "            " + (userRole <= sale_role.chief ? "AND S.chief_no = #chief_no " : "AND S.lead_no = #chief_no ") +
                        "            AND (#cust_type = C.cust_type OR #cust_type IS NULL) " +
                        label_flagsCond +
                        "        GROUP BY C.sale_no, S.sale_ename, C.cust_no, DATEPART(MONTH, tc_date) " +

                        "        UNION ALL " +

                        "        SELECT C.sale_no, S.sale_ename, C.cust_no, DATEPART(MONTH, r_date) as s_month, SUM(- d.packing_type * d.qty) AS TotalWeight " +
                        "        FROM r_master m " +
                        "            INNER JOIN r_detail D ON M.r_no = D.r_no " +
                        "            INNER JOIN customer C ON M.cust_no = C.cust_no " +
                        "            INNER JOIN sale_man S ON S.sale_no = C.sale_no " +
                        "        WHERE r_date >= #tc_date1 and r_date < #tc_date2 " +
                        "            " + (userRole <= sale_role.chief ? "AND S.chief_no = #chief_no " : "AND S.lead_no = #chief_no ") +
                        "            AND (#cust_type = C.cust_type OR #cust_type IS NULL) " +
                        label_flagsCond +
                        "        GROUP BY C.sale_no, S.sale_ename, C.cust_no, DATEPART(MONTH, r_date) " +
                        "    ) Q2 GROUP BY sale_no, sale_ename, cust_no, s_month " +
                        ") Q1 PIVOT (SUM(TotalWeight) FOR s_month IN ([1], [2], [3], [4], [5], [6], [7], [8], [9], [10], [11], [12])) AS PVT1 " +
                        "ORDER BY sale_no, cust_no "
                        );

                    Log.Debug($"[{GetCurrentUserSaleNo()}] {nameof(GetChiefReport)} - sql: {sql}");

                    // ==================================
                    // Read sale data
                    detailRows = db.Query(sql, new
                    {
                        tc_date1 = yearStart,
                        tc_date2 = yearEnd,
                        chief_no = chiefNo,
                        model.cust_type,
                    }).ToList();

                    db.Close();
                }

                var chiefQueryResult = Slapper.AutoMapper.MapDynamic<ChiefQueryResult>(detailRows).ToList();

                Debug.WriteLine($"{nameof(GetChiefReport)} - count: {chiefQueryResult.Count}");

                var resultModel = new ChiefReportResultModel();

                // Group result to ChiefReport_SaleMan(s)
                resultModel.SaleMans = chiefQueryResult.GroupBy(s => s.sale_no).Select(g1 => new ChiefReport_SaleMan
                {
                    sale_no = g1.Key,
                    sale_ename = g1.FirstOrDefault()?.sale_ename,
                    Agents = g1.Select(s => new ChiefReport_Agent
                    {
                        cust_no = s.cust_no,
                        MonthSales = new Dictionary<int, double>
                        {
                            {1, s.M1},
                            {2, s.M2},
                            {3, s.M3},
                            {4, s.M4},
                            {5, s.M5},
                            {6, s.M6},
                            {7, s.M7},
                            {8, s.M8},
                            {9, s.M9},
                            {10, s.M10},
                            {11, s.M11},
                            {12, s.M12}
                        }.ToList().Take(monthCount).ToDictionary(kv => kv.Key, kv => kv.Value)
                    }).ToList()
                }).ToList();

                // ==================================
                // Calculate Month Totals & Agent Total

                foreach (var saleman in resultModel.SaleMans)
                {
                    // Select MonthSales -> Group by Month -> Sum Sales of Month -> To Dict
                    saleman.MonthTotals = saleman.Agents.SelectMany(a => a.MonthSales)
                        .GroupBy(monthSale => monthSale.Key, monthSale => monthSale.Value)
                        .Select(ms => new { ms.Key, Sum = ms.Sum(s => s) }).ToDictionary(ms => ms.Key, ms => ms.Sum);
                }

                // Calculate all Grand Total.
                var months = Enumerable.Range(1, monthCount).ToList();
                var monthTotals = months.ToDictionary(m => m, m => .0);
                foreach (var saleMan in resultModel.SaleMans)
                {
                    foreach (var k in months)
                    {
                        if (saleMan.MonthTotals.ContainsKey(k))
                            monthTotals[k] += saleMan.MonthTotals[k];
                    }
                }

                resultModel.chief_ename = chief.sale_ename;

                var row = 1;
                var list = new Dictionary<int, object[]>();

                foreach (var saleMan in resultModel.SaleMans)
                {
                    for (var j = 0; j < saleMan.Agents.Count; j++)
                    {
                        var agent = saleMan.Agents[j];

                        list.Add(row++, new object[]
                        {
                            // Saleman Name
                            j == 0 ? saleMan.sale_ename : "",
                            // Agent
                            agent.cust_no
                            // Customer Months
                        }.Concat(agent.MonthSales.Select(kv => (object)kv.Value))
                            // Customer Total
                            .Concat(new[] { (object)agent.Total })
                            .ToArray());
                    }

                    // Saleman total
                    list.Add(row++, new object[]
                    {
                        "", "Total"
                    }.Concat(saleMan.MonthTotals.Select(kv => (object)kv.Value))
                        .Concat(new[] { (object)saleMan.MonthTotals.Sum(kv => kv.Value) })
                        .ToArray());
                }

                // Report Total line
                list.Add(row++, new object[]
                {
                    "Grand Total:", ""
                }.Concat(monthTotals.Select(kv => (object)kv.Value))
                    .Concat(new[] { (object)monthTotals.Sum(kv => kv.Value) })
                    .ToArray());

                // Apply format
                foreach (var item in list)
                {
                    for (var i = 2; i < item.Value.Length; i++)
                    {
                        if (item.Value[i] is double)
                            item.Value[i] = ((double)item.Value[i]).ToString("##,###");
                    }
                }

                var resultModel2 = new
                {
                    resultModel.chief_ename,
                    index =
                        new[] { "Sale Man", "Agent" }.Concat(months.ConvertAll(i => year + "." + i))
                            .Concat(new[] { "Grand Total" }),
                    data = list,
                };

                sw.Stop();
                Log.Info($"[{GetCurrentUserSaleNo()}] {nameof(GetChiefReport)} time: {sw.ElapsedMilliseconds}ms");

                return Content(JsonConvert.SerializeObject(new ApiResult<object>
                {
                    Result = resultModel2,
                    ResultCode = ApiResultCode.Success
                }), "application/json");
            }
            catch (Exception ex)
            {
                Log.Error($"[{GetCurrentUserSaleNo()}] {nameof(GetChiefReport)} exception: {ex}");

                return Content(JsonConvert.SerializeObject(new ApiResult<object>
                {
                    Result = null,
                    ResultCode = ApiResultCode.GenericError,
                    ResultMessages = new List<string> { ex.Message }
                }), "application/json");
            }
        }

        [Authorize]
        public ActionResult GetDirectorReport(DirectorReportModel model)
        {
            try
            {
                var sw = new Stopwatch();
                sw.Start();

                List<dynamic> detailRows;
                sale_man director;

                var saleNo = GetCurrentUserSaleNo();

                // Check permission
                var userRole = GetCurrentUserRole();
                if (userRole > sale_role.dir)
                {
                    // Check again if this user is not supporting director
                    if (!AppSettings.SupportingDirBrands.ContainsKey(saleNo))
                    {
                        return Content(JsonConvert.SerializeObject(new ApiResult
                        {
                            ResultCode = ApiResultCode.GenericError,
                            ResultMessages = new List<string> { "Unauthorized Access" },
                        }), "application/json");
                    }
                }

                var dirNo = saleNo;
                if (GetCurrentUserRole() == sale_role.gen)
                    dirNo = model.sale_no;
                else if (AppSettings.SupportingDirBrands.ContainsKey(saleNo))
                    dirNo = AppSettings.SupportingDirs[saleNo];

                // set model.label_flag
                model.label_flag = userRole <= sale_role.dir ? null : AppSettings.SupportingDirBrands[dirNo].First();

                // Back 1 cycle from periodMid
                DateTime range1A, range1B;
                // Start of priod (month/week/year)
                DateTime periodMid;
                DateTime theDay;
                // End of priod
                DateTime range2A, range2B;

                // Daily
                if (model.PeriodType == PeriodType.Daily)
                {
                    periodMid = new DateTime(model.tc_date.Year, model.tc_date.Month, 1);
                    range1A = periodMid.AddMonths(-1);
                    // End of Selected date of Last month
                    range1B = model.tc_date.Date.AddMonths(-1).AddDays(1);
                    range2A = periodMid;
                    // End of Selected date of this month
                    range2B = model.tc_date.Date.AddDays(1);
                    // Start of selected date
                    theDay = model.tc_date.Date;
                }
                // Monthly
                else if (model.PeriodType == PeriodType.Monthly)
                {
                    periodMid = new DateTime(model.tc_date.Year, model.tc_date.Month, 1);
                    range1A = periodMid.AddMonths(-1);
                    range1B = periodMid;
                    range2A = periodMid;
                    range2B = periodMid.AddMonths(1);
                    theDay = range2B;
                }
                // Quarterly
                else if (model.PeriodType == PeriodType.Quarterly)
                {
                    var month = model.tc_date.Month;
                    // 0-based -> Quarter -> Month-0 -> Month-1.
                    month = ((month - 1) / 3) * 3 + 1;

                    periodMid = new DateTime(model.tc_date.Year, month, 1);
                    range1A = periodMid.AddMonths(-3);
                    range1B = periodMid;
                    range2A = periodMid;
                    range2B = periodMid.AddMonths(3);
                    theDay = range2B;
                }
                // Anually
                else if (model.PeriodType == PeriodType.Annualy)
                {
                    periodMid = new DateTime(model.tc_date.Year, 1, 1);
                    range1A = periodMid.AddYears(-1);
                    range1B = periodMid;
                    range2A = periodMid;
                    range2B = periodMid.AddYears(1);
                    theDay = range2B;
                }
                else
                {
                    return Content(JsonConvert.SerializeObject(new ApiResult
                    {
                        ResultCode = ApiResultCode.GenericError,
                        ResultMessages = new List<string> { "Invalid PeriodType parameter." },
                    }), "application/json");
                }

                if (range2B > DateTime.Now.Date.AddDays(1))
                {
                    range2B = DateTime.Now.Date.AddDays(1);
                }

                // Clear lower level filter of P1
                if (string.IsNullOrWhiteSpace(model.p_1))
                    model.p_2 = null;
                if (string.IsNullOrWhiteSpace(model.p_2))
                    model.product_no = null;

                // Include ThisDay
                var withThisDay = model.PeriodType == PeriodType.Daily;

                using (
                    IDbConnection db =
                        new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    // ==================================
                    // Read customer
                    director = db.Query<sale_man>(_T("SELECT * FROM sale_man WHERE is_ok = '1' AND sale_no = #sale_no"),
                        new
                        {
                            sale_no = dirNo,
                        }).FirstOrDefault();

                    if (director == null)
                        return
                            Content(
                                JsonConvert.SerializeObject(new ApiResult
                                {
                                    ResultCode = ApiResultCode.ParametersError,
                                    ResultMessages = new List<string> { "Director not found." }
                                }), "application/json");

                    // Include product table for filtering
                    //var withProduct =
                    //    !(string.IsNullOrEmpty(model.p_1) && string.IsNullOrEmpty(model.p_2) &&
                    //      string.IsNullOrEmpty(model.product_no));

                    var sql = _T(
                        "SELECT chief_no, chief_ename, sale_no, sale_ename, mobile, " +
                        "	SUM(LastPeriod) LastPeriod, " +
                        (withThisDay ?
                        "	SUM(ThisDay) ThisDay, " : "") +
                        "	SUM(ThisPeriod) ThisPeriod " +
                        "FROM ( " +
                        "	SELECT sale_no, sale_ename, mobile, " +
                        "		chief_no, chief_ename, cust_no, ISNULL([LastPeriod], 0) [LastPeriod], " +
                        (withThisDay ?
                        "		ISNULL(ThisDay, 0) ThisDay, " : "") +
                        "		ISNULL(ThisPeriod, 0) " + (withThisDay ? " + ISNULL(ThisDay, 0)" : "") + " ThisPeriod " +
                        "	FROM ( " +
                        "        SELECT sale_no, sale_ename, mobile, chief_no, chief_ename, cust_no, RangeType, SUM(TotalWeight) AS TotalWeight " +
                        "        FROM ( " +
                        "            SELECT s.sale_no, s.sale_ename, s.mobile, " +
                        "                s.chief_no, f.sale_ename chief_ename, m.cust_no, " +
                        "                CASE WHEN tc_date >= @Range1A AND tc_date < @Range1B THEN 'LastPeriod' " +
                        "                     WHEN tc_date >= @Range2A AND tc_date < @theday THEN 'ThisPeriod' " +
                        (withThisDay ?
                        "                     WHEN tc_date >= @theday AND tc_date < @Range2B THEN 'ThisDay' " : "") +
                        "                     ELSE 'Other' END AS RangeType, " +
                        "                d.packing_type * d.qty AS TotalWeight " +
                        "            FROM tc_master m " +
                        "                INNER JOIN tc_detail d ON m.tc_no = d.tc_no " +
                        "                INNER JOIN customer c ON m.cust_no = c.cust_no " +
                        "                INNER JOIN sale_man s ON c.sale_no = s.sale_no AND s.dir_no = @dir_no " +
                        "                INNER JOIN sale_man f ON s.chief_no = f.sale_no " +
                        "                INNER JOIN product p ON d.product_no = p.product_no " +
                        "            WHERE tc_date >= @Range1A AND tc_date < @Range2B " +
                        "                AND (c.label_flag = @label_flag OR @label_flag IS NULL) " +
                        "                AND (p.p_1 = @p_1 OR @p_1 IS NULL) " +
                        "                AND (p.p_2 = @p_2 OR @p_2 IS NULL) " +
                        "                AND (p.product_no = @product_no OR @product_no IS NULL) " +

                        "            UNION ALL " +

                        "            SELECT s.sale_no, s.sale_ename, s.mobile, " +
                        "                s.chief_no, f.sale_ename chief_ename, m.cust_no, " +
                        "                CASE WHEN r_date >= @Range1A AND r_date < @Range1B THEN 'LastPeriod' " +
                        "                     WHEN r_date >= @Range2A AND r_date < @theday THEN 'ThisPeriod' " +
                        (withThisDay ?
                        "                     WHEN r_date >= @theday AND r_date < @Range2B THEN 'ThisDay' " : "") +
                        "                     ELSE 'Other' END AS RangeType, " +
                        "                - d.packing_type * d.qty AS TotalWeight " +
                        "            FROM r_master m " +
                        "                INNER JOIN r_detail d ON m.r_no = d.r_no " +
                        "                INNER JOIN customer c ON m.cust_no = c.cust_no " +
                        "                INNER JOIN sale_man s ON c.sale_no = s.sale_no AND s.dir_no = @dir_no " +
                        "                INNER JOIN sale_man f ON s.chief_no = f.sale_no " +
                        "                INNER JOIN product p ON d.product_no = p.product_no " +
                        "            WHERE r_date >= @Range1A AND r_date < @Range2B " +
                        "                AND (c.label_flag = @label_flag OR @label_flag IS NULL) " +
                        "                AND (p.p_1 = @p_1 OR @p_1 IS NULL) " +
                        "                AND (p.p_2 = @p_2 OR @p_2 IS NULL) " +
                        "                AND (p.product_no = @product_no OR @product_no IS NULL) " +
                        "        ) Q2 GROUP BY sale_no, sale_ename, mobile, chief_no, chief_ename, cust_no, RangeType " +
                        "    ) Q1 PIVOT (SUM(TotalWeight) FOR RangeType IN ([LastPeriod], " + (withThisDay ? "[ThisDay], " : "") + "[ThisPeriod])) AS PVT1 " +
                        ") PVT " +
                        "GROUP BY chief_no, chief_ename, sale_no, sale_ename, mobile " +
                        "ORDER BY chief_ename, sale_ename "
                        );

                    var queryParams = new
                    {
                        // In normal cases (non-daily), theDay must = range2B
                        range1A,
                        range1B,
                        range2A,
                        range2B,
                        theDay,
                        dir_no = dirNo,
                        model.label_flag,
                        model.p_1,
                        model.p_2,
                        model.product_no,
                    };

                    Log.Debug($"[{GetCurrentUserSaleNo()}] {nameof(GetDirectorReport)} - query params: {JsonConvert.SerializeObject(queryParams)}");
                    Log.Debug($"[{GetCurrentUserSaleNo()}] {nameof(GetDirectorReport)} - sql: {sql}");

                    detailRows = db.Query(sql, queryParams).ToList();

                    db.Close();
                }

                var queryResult = Slapper.AutoMapper.MapDynamic<DirectorQueryResult>(detailRows).ToList();

                // Details list
                var resultModel = new DirectorReportResultModel
                {
                    Chiefs = queryResult.GroupBy(s => s.chief_no).Select(g1 => new DirectorReport_Chief
                    {
                        sale_no = g1.Key,
                        sale_ename = g1.FirstOrDefault()?.chief_ename,
                        Salemans = g1.Select(s => new DirectorReport_Saleman
                        {
                            sale_no = s.sale_no,
                            sale_ename = s.sale_ename,
                            mobile = s.mobile,
                            ThisDaySale = s.ThisDay,
                            LastPeriodSale = s.LastPeriod,
                            ThisPeriodSale = s.ThisPeriod,
                        }).ToList()
                    }).ToList(),
                };

                Debug.WriteLine($"{nameof(GetDirectorReport)} - count: {resultModel.Chiefs.Count}");

                // ==================================
                // Finish highest level info

                resultModel.director_ename = director.sale_ename;

                // ==================================
                // Convert to new data format
                var row = 1;
                var list = new Dictionary<int, object[]>();

                foreach (var chief in resultModel.Chiefs)
                {
                    foreach (var saleMan in chief.Salemans)
                    {
                        List<object> tmpRow = new List<object>
                        {

                            "", // Chief
                            saleMan.sale_no + " " + saleMan.sale_ename, // Saleman
                            //saleMan.ThisDaySale,
                            saleMan.ThisPeriodSale,
                            saleMan.LastPeriodSale,
                            saleMan.Offset,
                            saleMan.PercentOffset,
                        };
                        if (withThisDay)
                            tmpRow.Insert(2, saleMan.ThisDaySale);
                        list.Add(row++, tmpRow.ToArray());
                    }

                    List<object> tmpRow2 = new List<object>
                    {
                        chief.sale_no + " " + chief.sale_ename, // Chief
                        "", // Saleman
                        //chief.ThisDaySale,
                        chief.ThisPeriodSale,
                        chief.LastPeriodSale,
                        chief.Offset,
                        chief.PercentOffset,
                    };
                    // Chief Total
                    if (withThisDay)
                        tmpRow2.Insert(2, chief.ThisDaySale);
                    list.Add(row++, tmpRow2.ToArray());
                }

                List<object> tmpRow3 = new List<object>
                {
                    "",
                    "TOTAL:",
                    //resultModel.ThisDaySale,
                    resultModel.ThisPeriodSale,
                    resultModel.LastPeriodSale,
                    "", ""
                };
                if (withThisDay)
                    tmpRow3.Insert(2, resultModel.ThisDaySale);
                list.Add(row++, tmpRow3.ToArray());

                // =====================================================================
                // Apply format

                foreach (var item in list)
                {
                    for (var i = 2; i < item.Value.Length; i++)
                    {
                        if (!(item.Value[i] is double))
                            continue;

                        if (i >= item.Value.Length - 1)
                        {
                            item.Value[i] = ((double)item.Value[i]).ToString("##%");
                        }
                        else
                        {
                            item.Value[i] = ((double)item.Value[i]).ToString("##,###");
                        }
                    }
                }

                // =====================================================================
                // API Output

                var periodName = Periods[model.PeriodType];

                var resultModel2 = new
                {
                    resultModel.director_ename,
                    //from_date = periodStart.ToString("yyyy-MM-dd"),
                    from_date = model.tc_date.Date.ToString("yyyy-MM-dd"),
                    to_date = range2B.ToString("yyyy-MM-dd"),
                    index = withThisDay
                        ? new object[]
                        {
                            "Trưởng vùng", "Tiếp thị", "This day (Kgs)",
                            $"This {periodName} (Kgs)", $"Last {periodName} (Kgs)", "Offset", "Percent Offset"
                        }
                        : new object[]
                        {
                            "Trưởng vùng", "Tiếp thị",
                            $"This {periodName} (Kgs)", $"Last {periodName} (Kgs)", "Offset", "Percent Offset"
                        },
                    data = list,
                };

                sw.Stop();
                Log.Info($"[{GetCurrentUserSaleNo()}] {nameof(GetCustomerReport)} time: {sw.ElapsedMilliseconds}ms");

                return Content(JsonConvert.SerializeObject(new ApiResult<object>
                {
                    Result = resultModel2,
                    ResultCode = ApiResultCode.Success
                }), "application/json");
            }
            catch (Exception ex)
            {
                Log.Error($"[{GetCurrentUserSaleNo()}] {nameof(GetDirectorReport)} exception: {ex}");

                return
                    Content(
                        JsonConvert.SerializeObject(new ApiResult<object>
                        {
                            //Result = null,
                            ResultCode = ApiResultCode.GenericError,
                            ResultMessages = new List<string> { ex.Message }
                        }), "application/json");
            }
        }

        /// <summary>
        /// Báo cáo "Product Margin" cho giám đốc
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult GetProductMarginReport(ProductMarginReportModel model)
        {
            try
            {
                var sw = new Stopwatch();
                sw.Start();

                var saleNo = GetCurrentUserSaleNo();

                // Check permission
                var userRole = GetCurrentUserRole();
                if (userRole > sale_role.dir)
                {
                    // Check again if this user is not supporting director
                    if (!AppSettings.SupportingDirBrands.ContainsKey(saleNo))
                    {
                        return Content(JsonConvert.SerializeObject(new ApiResult
                        {
                            ResultCode = ApiResultCode.GenericError,
                            ResultMessages = new List<string> { "Unauthorized Access" },
                        }), "application/json");
                    }
                }

                IEnumerable<ProductMarginReportQueryResult> queryResult;

                using (var db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    var sql = _T(
                        "SELECT big_table.p_1, big_table.big_name, medium_table.p_2, medium_table.medium_name, " +
                        "	product.product_no, product.product_vname, product_margin.packing_type, " +
                        "	product_margin.qty, product_margin.unit_margin, " +
                        "	SUM(product_margin.unit_margin * product_margin.qty) AS margin " +
                        "FROM product_margin " +
                        "	INNER JOIN product ON product_margin.product_no = product.product_no " +
                        "	INNER JOIN medium_table ON product.p_2 = medium_table.p_2 " +
                        "	INNER JOIN big_table ON medium_table.p_1 = big_table.p_1 " +
                        "GROUP BY big_table.p_1, medium_table.p_2, product.product_no, big_table.big_name, " +
                        "	medium_table.medium_name, product.product_vname, product_margin.packing_type, " +
                        "	product_margin.qty, product_margin.unit_margin, product_margin.the_month " +
                        "HAVING        (product_margin.the_month = @the_month) " +
                        "ORDER BY big_table.p_1, medium_table.p_2, " +
                        "	product.product_no, product_margin.packing_type "
                        );

                    Log.Debug($"[{GetCurrentUserSaleNo()}] {nameof(GetDirectorReport)} - sql: {sql}");

                    // Parse report month: month-year
                    var parts = model.report_month.Split('-');

                    queryResult = db.Query<ProductMarginReportQueryResult>(sql, new
                    {
                        // Month: 2018.01 
                        the_month = $"{parts[1]}.{parts[0]}"
                    }).ToList();

                    db.Close();
                }

                // Group{p1: {p2: row}}
                var resultGroups = queryResult.GroupBy(r => new { r.p_1, r.p_2 }).GroupBy(r => r.Key.p_1);

                // List of rows
                var row = 1;
                var rowList = new Dictionary<int, object[]>();
                double ttQtySum = 0;
                double ttMarginSum = 0;
                double ttMarginUnit = 0;

                resultGroups.ForEach(p1 =>
                {
                    double p1QtySum = 0;
                    double p1MarginSum = 0;
                    double p1MarginUnit = 0;

                    p1.ForEach(p2 =>
                    {
                        double p2QtySum = 0;
                        double p2MarginSum = 0;
                        double p2MarginUnit = 0;

                        p2.ForEach(pr =>
                        {
                            // Columns: Product Name, PP Type, Sale Qty, Unit Margin, Margin
                            var rowUnit = pr.qty != 0 ? pr.margin / pr.qty : 0;

                            rowList.Add(row++, new object[]
                            {
                                $"{pr.product_no} {pr.product_vname}",
                                pr.packing_type,
                                pr.qty.ToString("##,###"),
                                rowUnit.ToString("##,###"), // pr.unit_margin
                                pr.margin.ToString("##,###"),
                            });
                        });

                        p1QtySum += (p2QtySum = p2.Sum(g2 => g2.qty));
                        p1MarginSum += (p2MarginSum = p2.Sum(g2 => g2.margin));
                        p2MarginUnit = p2QtySum != 0 ? p2MarginSum / p2QtySum : 0;

                        // p2 sum
                        rowList.Add(row++, new object[]
                        {
                            $"{p2.Key.p_2} Total",
                            "", // pr.packing_type
                            p2QtySum.ToString("##,###"), // qty
                            p2MarginUnit.ToString("##,###"), // unit_margin
                            p2MarginSum.ToString("##,###"), // margin
                        });
                    });

                    ttQtySum += p1QtySum;
                    ttMarginSum += p1MarginSum;
                    p1MarginUnit = p1QtySum != 0 ? p1MarginSum / p1QtySum : 0;

                    // p1 sum
                    rowList.Add(row++, new object[]
                    {
                        $"{p1.Key} Total",
                        "", // pr.packing_type
                        p1QtySum.ToString("##,###"), // qty
                        p1MarginUnit.ToString("##,###"), // unit_margin
                        p1MarginSum.ToString("##,###"), // margin
                    });
                });

                Debug.WriteLine($"{nameof(GetProductMarginReport)} - count: {resultGroups.Count()}");

                // ====================================
                // Add Total line
                ttMarginUnit = ttQtySum != 0 ? ttMarginSum / ttQtySum : 0;
                rowList.Add(row++, new object[]
                {
                    "TOTAL:",
                    "", // pr.packing_type
                    ttQtySum.ToString("##,###"), // qty
                    ttMarginUnit.ToString("##,###"), // unit_margin
                    ttMarginSum.ToString("##,###"), // margin
                });

                sw.Stop();
                Log.Info($"[{GetCurrentUserSaleNo()}] {nameof(GetCustomerReport)} time: {sw.ElapsedMilliseconds}ms");

                // =====================================================================
                // API Output

                var resultModel2 = new
                {
                    index = new object[]
                    {
                        "Product Name", "P.P Type", "Sale Quantity", "Unit Margin", "Margin"
                    },
                    data = rowList,
                };

                return Content(JsonConvert.SerializeObject(new ApiResult<object>
                {
                    Result = resultModel2,
                    ResultCode = ApiResultCode.Success
                }), "application/json");
            }
            catch (Exception ex)
            {
                Log.Error($"[{GetCurrentUserSaleNo()}] {nameof(GetDirectorReport)} exception: {ex}");

                return Content(JsonConvert.SerializeObject(new ApiResult<object>
                {
                    ResultCode = ApiResultCode.GenericError,
                    ResultMessages = new List<string> { ex.Message }
                }), "application/json");
            }
        }

        #endregion

        private string _T(string sql)
        {
            var oracle = false;

            return sql.Replace("#", oracle ? ":" : "@");
        }

        //private void TestOracle()
        //{
        //    using (IDbConnection db =
        //        new OracleConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
        //    {
        //        var employees = db.Query<Employee>(_T("SELECT first_name, last_name FROM Employees"));
        //        Debug.WriteLine("Count: " + employees.Count());
        //    }
        //}
    }

    public class Employee
    {
        public string first_name { get; set; }

        public string last_name { get; set; }
    }

}
