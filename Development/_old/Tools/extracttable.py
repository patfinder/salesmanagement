__author__ = 'vuong'

import csv
import json
import sys
import traceback

fpi = open(sys.argv[1], 'rt')

jsonObj = json.load(fpi, encoding='utf8')

index = jsonObj['Result']['index']
dataArr = jsonObj['Result']['data']

# print(json.dumps(dataArr))

# Open in wb mode to exclude extra blank line
fpo = open(sys.argv[1] + ".csv", 'wb')
# fpo = open(sys.argv[1] + ".csv", 'w', newline='')
csvWriter = csv.writer(fpo, quoting=csv.QUOTE_ALL)

csvWriter.writerow(index)

for key in iter(sorted(map(int, dataArr.keys()))):
    try:
        # row = [str(col).decode("windows-1252").encode("utf8") for col in dataArr[str(key)]]
        row = []
        for col in dataArr[str(key)]:
            try:
                decoded = str(col).decode("windows-1252")
                row += [decoded.encode("utf8")]
            except:
                row += ["",]

        # row = [str(col).encode('ascii', errors='backslashreplace') for col in dataArr[str(key)]]
        # row = dataArr[str(key)]
        # csvWriter.writerow([key,] + row)
    except Exception as ex:

        # print "Exception: %s" % ex
        traceback.print_exc()
        print "----------------------------------"
        continue

    if len(row) > 1:
        csvWriter.writerow(row)
