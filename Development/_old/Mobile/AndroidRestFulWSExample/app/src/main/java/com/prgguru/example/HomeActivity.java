package com.prgguru.example;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.PersistentCookieStore;
import com.loopj.android.http.RequestParams;

/**
 * 
 * Home Screen Activity
 */
public class HomeActivity extends Activity // ListActivity implements LoaderManager.LoaderCallbacks<Cursor>
{
    ProgressDialog prgDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

        //Displays Home Screen
        setContentView(R.layout.home);

        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Loading data. Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);

        Button button = (Button) findViewById(R.id.btnLoadData);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                doGetData();
            }
        });
	}

    protected void doGetData() {

        prgDialog.show();

        PersistentCookieStore myCookieStore = new PersistentCookieStore(this);

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.setCookieStore(myCookieStore);
        client.setTimeout(30000);

        // Instantiate Http Request Param Object
        RequestParams params = new RequestParams();

        // client.get("http://192.168.43.17:9999/useraccount/login/dologin",params ,new AsyncHttpResponseHandler() {
        // client.post("http://192.168.1.101:82/home/GetData", params, new AsyncHttpResponseHandler() {
        client.post("http://137.116.131.7:82/home/GetData?agentName=" + LoginActivity.Email, params, new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response) {
                // Hide Progress Dialog
                prgDialog.hide();

                try {

                    Gson gson = new Gson();
                    SaleData saleData = gson.fromJson(response, SaleData.class);

                    String textContent = "";

                    for (SaleDataItem item : saleData.Items) {
                        textContent += item.ShopName + ": " + item.SaleVolumn + "\n";
                    }

                    TextView textView = (TextView) findViewById(R.id.textView);
                    textView.setText(textContent);

                    Toast.makeText(getApplicationContext(), "Get data successfully: " + saleData.AgentName, Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error,
                                  String content) {
                // Hide Progress Dialog
                prgDialog.hide();
                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

//    @Override
//    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
//        return null;
//    }
//
//    @Override
//    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
//
//    }
//
//    @Override
//    public void onLoaderReset(Loader<Cursor> loader) {
//
//    }
}
